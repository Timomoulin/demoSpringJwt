package com.example.demoJwt.services;

import com.example.demoJwt.entities.Role;
import com.example.demoJwt.repositories.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {
    private RoleRepository roleRepository;
@Autowired
    public RoleService(RoleRepository roleRepository) {
        this.roleRepository = roleRepository;
    }

    public Role save(Role role){
        return this.roleRepository.save(role);
    }
}
