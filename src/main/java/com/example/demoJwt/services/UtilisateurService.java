package com.example.demoJwt.services;

import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demoJwt.dtos.UtilisateurDTO;
import com.example.demoJwt.entities.Utilisateur;
import com.example.demoJwt.exceptions.DuplicationException;
import com.example.demoJwt.exceptions.NotFoundException;
import com.example.demoJwt.repositories.UtilisateurRepository;

@Service
public class UtilisateurService {
	
	@Autowired
	private UtilisateurRepository repository;
	
	public Utilisateur findById(Long id) {
		return repository.findById(id).orElseThrow(
				() -> new NotFoundException("Person not found: " + id));
	}
	
	public Utilisateur findByEmail(String email) {
		return repository.findByEmail(email).orElseThrow(
				() -> new NotFoundException("Person not found: " + email));
	}
	
	public List<Utilisateur> findAll() {
		return repository.findAll();
	}
	
	public Utilisateur create(Utilisateur utilisateur) {
		utilisateur.setId(null);
		checkEmailDuplication(utilisateur);
		return repository.save(utilisateur);
	}
	
	public UtilisateurDTO create(UtilisateurDTO dto) {
		return new UtilisateurDTO(create(new Utilisateur(dto)));
	}
	
	public Utilisateur update(Utilisateur utilisateur) {
		checkEmailDuplication(utilisateur);
		Utilisateur p = findById(utilisateur.getId());
		p.setName(utilisateur.getName());
		p.setEmail(utilisateur.getEmail());
		p.setRole(utilisateur.getRole());
		return repository.save(p);
	}
	
	public void delete(Long id) {
		final Utilisateur p = findById(id);
		repository.delete(p);
	}
	
	private void checkEmailDuplication(Utilisateur utilisateur) {
		final String email = utilisateur.getEmail();
		if (email != null && email.length() > 0) {
			final Long id = utilisateur.getId();
			final Utilisateur p = repository.findByEmail(email).orElse(null);
			if (p != null && Objects.equals(p.getEmail(), email) && !Objects.equals(p.getId(), id)) {
				throw new DuplicationException("Email duplication: " + email);
			}
		}
	}

}
