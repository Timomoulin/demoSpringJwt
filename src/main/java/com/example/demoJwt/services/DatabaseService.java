package com.example.demoJwt.services;

import com.example.demoJwt.entities.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.example.demoJwt.entities.Utilisateur;
@Service
public class DatabaseService {

	private UtilisateurService utilisateurService; // Service pour gérer les utilisateurs
	private RoleService roleService; // Service pour gérer les rôles
	private PasswordEncoder passwordEncoder; // Utilitaire pour encoder les mots de passe

	@Autowired
	public DatabaseService(
			UtilisateurService utilisateurService,
			RoleService roleService,
			PasswordEncoder passwordEncoder) {
		this.utilisateurService = utilisateurService;
		this.roleService = roleService;
		this.passwordEncoder = passwordEncoder;
	}

	public void initializeDatabase() {
		// Méthode pour initialiser la base de données avec des rôles et des utilisateurs par défaut

		System.out.println("Initializing database...");

		final Role roleAdmin = new Role(null, "Admin");
		final Role roleClient = new Role(null, "Client");

		final Utilisateur user1 = new Utilisateur("Test Admin", "admin@mail.com", passwordEncoder.encode("admin@mail.com"));
		final Utilisateur user2 = new Utilisateur("Test Client", "client@mail.com", passwordEncoder.encode("client@mail.com"));
		// Crée des objets de rôle et d'utilisateur avec des valeurs par défaut

		System.out.println(roleService.save(roleAdmin));
		System.out.println(roleService.save(roleClient));
		// Enregistre les rôles dans la base de données et obtient les identifiants générés

		user1.setRole(roleAdmin);
		user2.setRole(roleClient);
		// Associe les rôles aux utilisateurs

		System.out.println(utilisateurService.create(user1));
		System.out.println(utilisateurService.create(user2));
		// Enregistre les utilisateurs dans la base de données

		System.out.println("Database initialized!");
		// Affiche un message pour indiquer que l'initialisation de la base de données est terminée
	}
}