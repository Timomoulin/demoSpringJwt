package com.example.demoJwt.entities;

import java.io.Serializable;

import jakarta.persistence.*;

import com.example.demoJwt.dtos.UtilisateurDTO;

@Entity
public class Utilisateur implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)

	private Long id;
	
	@Column(nullable = false, length = 50)
	private String name;
	
	@Column(nullable = false, unique = true, length = 100)
	private String email;
	
	@Column(nullable = false, length = 60)
	private String password;

	@ManyToOne
	@JoinColumn(name = "role_id")
	private Role role;

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Utilisateur() {
		super();
	}


	public Utilisateur(String name, String email, String password) {
		super();
		this.name = name;
		this.email = email;
		this.password = password;
	}

	public Utilisateur(Long id, String name, String email, String password, Role role) {
		this.id = id;
		this.name = name;
		this.email = email;
		this.password = password;
		this.role = role;
	}

	public  Utilisateur(UtilisateurDTO utilisateurDTO){
		this.id= utilisateurDTO.getId();
		this.name= utilisateurDTO.getName();
		this.email = utilisateurDTO.getEmail();
		this.password = utilisateurDTO.getPassword();
		this.role= utilisateurDTO.getRole();
	}

	public Long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public String getEmail() {
		return email;
	}



	public String getPassword() {
		return password;
	}



	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Utilisateur{" +
				"id=" + id +
				", name='" + name + '\'' +
				", email='" + email + '\'' +
				", role=" + role +
				'}';
	}
}
