package com.example.demoJwt.controllers;

import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/test")
public class MainController {

    @GetMapping("/visiteur")
    public ResponseEntity<String> visiteur() {
        return ResponseEntity.ok("Visible pour tous");
    }

    @GetMapping("/client")
    public ResponseEntity<String> client() {
        return ResponseEntity.ok("Visible pour les clients ou admins");
    }

    @GetMapping("/admin")
    public ResponseEntity<String> admin() {
        return ResponseEntity.ok("Visible pour les admins");
    }
}
