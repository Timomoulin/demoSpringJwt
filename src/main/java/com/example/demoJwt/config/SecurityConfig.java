package com.example.demoJwt.config;

import com.example.demoJwt.services.DatabaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.configuration.AuthenticationConfiguration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class SecurityConfig {

	@Autowired
	private UserDetailsService userDetailsService; // Service pour gérer les détails des utilisateurs

	@Autowired
	private JwtAuthFilter jwtAuthFilter; // Filtre d'authentification basé sur JWT

	@Bean
	public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity) throws Exception {
		return httpSecurity
				.csrf().disable() // Désactive la protection CSRF
				.authorizeHttpRequests()
				.requestMatchers("/test/visiteur","/auth/**").permitAll() // Autorise les URL publiques sans authentification
				.requestMatchers("/test/client/**").hasAnyAuthority("Client","Admin")
				.requestMatchers("/test/admin/**").hasAuthority("Admin") // Autorise les requêtes les utilisateurs avec l'autorité "Admin"
				.anyRequest().authenticated() // Exige l'authentification pour toutes les autres requêtes
				.and()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) // Configure la gestion des sessions comme STATELESS (sans état)
				.and()
				.authenticationProvider(authenticationProvider()) // Configure le fournisseur d'authentification personnalisé
				.addFilterBefore(jwtAuthFilter, UsernamePasswordAuthenticationFilter.class) // Ajoute le filtre d'authentification basé sur JWT avant le filtre par défaut
				.build();
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder(); // Utilise l'encodeur BCrypt pour les mots de passe
	}

	@Bean
	public AuthenticationManager authenticationManager(AuthenticationConfiguration config) throws Exception {
		return config.getAuthenticationManager(); // Configure le gestionnaire d'authentification
	}

	@Bean
	public WebSecurityCustomizer webSecurityCustomizer() {
		return (web) -> web.ignoring().requestMatchers(new AntPathRequestMatcher("/h2-console/**"));
		// Permet d'accéder à la console H2 sans authentification (utile pour le développement)
	}

	private AuthenticationProvider authenticationProvider() {
		final DaoAuthenticationProvider authenticationProvider = new DaoAuthenticationProvider();
		authenticationProvider.setUserDetailsService(userDetailsService);
		authenticationProvider.setPasswordEncoder(passwordEncoder());
		return authenticationProvider; // Configure le fournisseur d'authentification pour l'application
	}
}